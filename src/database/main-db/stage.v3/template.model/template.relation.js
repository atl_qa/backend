const { editor } = require("@nodespull/core/database")("main")
const { relations, onUpload, onRevert, rawQuery } = editor


onUpload(() => {
    relations.set(forTable = "template", {
        /** add FK to other tables */ has_one: [],
        /** add FK to other tables */ has_many: [],
        /** create join tables */ many_to_many: []
    })
})


onRevert(() => {
    relations.set(forTable = "template", {
        /** add FK to other tables */ has_one: [],
        /** add FK to other tables */ has_many: [],
        /** create join tables */ many_to_many: []
    })
})
