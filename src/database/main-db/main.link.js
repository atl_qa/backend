const { npLink } = require("@nodespull/core")
const { system } = require("@nodespull/core/utils/db")
const { sysEnv, appEnv } = require("@nodespull/core/env")


/**
 * database access configs
 */
npLink.database({
    isActive: true,
    system: system.mySQL,
    selector: sysEnv.db_selector,
    database: sysEnv.db_database,
    host: sysEnv.db_host,
    port: sysEnv.db_port,
    username: sysEnv.db_username,
    password: sysEnv.db_password
})
