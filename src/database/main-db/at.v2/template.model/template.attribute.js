const { editor } = require("@nodespull/core/database")("main")
const { type, onUpload, onRevert, rawQuery } = editor


onUpload(() => {
    editor.defineModel(withTable = "template").as({
        /* add attributes */
        uuid: {
            type: type.string,
            primaryKey: true,
            defaultValue: type.UUIDV1
        },
        name: type.string,
        value: type.text("long"),
        author: type.string
    })
})


onRevert(() => {
    editor.defineModel(withTable = "template").as({
        /* add attributes */
        uuid: {
            type: type.string,
            primaryKey: true,
            defaultValue: type.UUIDV1
        },
        name: type.string,
        value: type.text("long"),
        author: type.string
    })
})
