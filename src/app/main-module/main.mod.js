const { npModule } = require("@nodespull/core")
const { jwt } = require("@nodespull/core/crypt")


exports.mainModule = npModule({
    name: "mainModule",
    loadRoutes: true,
    useGuard: null,
    imports: []
})
