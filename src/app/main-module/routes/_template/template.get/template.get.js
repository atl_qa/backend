const { http, link, npRoute, pipe } = require("@nodespull/core")
const { hash, jwt, oauth2 } = require("@nodespull/core/crypt")
const { mainModule } = require("../../../main.mod")


const $ = npRoute({
    loader: mainModule,
    method: http.GET,
    handler: templateHandler,
    path: "/template",
    urlParams: [ "name" ]
})

/**
 * @param {Request}  req request contains client data
 * @param {Response} res response contains http methods
 */
async function templateHandler(req, res) {
    let db = link.get("main")
    let [rows, error] = await db.table("template").where({ name: req.params.name }).select()
    if(error) res.status(400).send()
    else if(rows.length > 0) res.json({template:rows[0]})
    else res.status(404).send()

}
