const { http, link, npRoute, pipe } = require("@nodespull/core")
const { hash, jwt, oauth2 } = require("@nodespull/core/crypt")
const { mainModule } = require("../../../../main.mod")


const $ = npRoute({
    loader: mainModule,
    method: http.GET,
    handler: allHandler,
    path: "/template/all",
    urlParams: []
})

/**
 * @param {Request}  req request contains client data
 * @param {Response} res response contains http methods
 */
async function allHandler(req, res) {
    
    let db = link.get("main")
    let [rows, error] = await db.table("template").select()
    console.log(error)
    if(error) res.status(400).send()
    else if(rows.length > 0) res.json({templates:rows})
    else res.status(404).send()

}
