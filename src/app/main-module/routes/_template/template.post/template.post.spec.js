const { http, link } = require("@nodespull/core")
const { mainModule } = require("../../../main.mod")
const { expect } = require('chai')



describe("POST: /template", () => {

    it("should return status 200", async () => {
        [status, data] = await mainModule.forward(req = {body:{
            name: "selenium",
            author: "test-actor",
            value: "a very long yml file"
        }}).to(http.POST, "/template")
        expect(status).to.equal(200)

    });


})