const { http, link, npRoute, pipe } = require("@nodespull/core")
const { hash, jwt, oauth2 } = require("@nodespull/core/crypt")
const { mainModule } = require("../../../main.mod")


const $ = npRoute({
    loader: mainModule,
    method: http.POST,
    handler: templateHandler,
    path: "/template",
    urlParams: []
})

/**
 * @param {Request}  req request contains client data
 * @param {Response} res response contains http methods
 */
async function templateHandler(req, res) {
    
    let db = link.get("main")
    let [row, err] = await db.table("template").insert(req.body)
    console.log(err)
    if(err) res.status(400).send()
    else if (row) res.status(200).json({template: row})
}
