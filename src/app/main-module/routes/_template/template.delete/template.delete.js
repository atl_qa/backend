const { http, link, npRoute, pipe } = require("@nodespull/core")
const { hash, oauth2, jwt } = require("@nodespull/core/crypt")
const { mainModule } = require("../../../main.mod")


const $ = npRoute({
    loader: mainModule,
    method: http.DELETE,
    handler: templateHandler,
    path: "/template",
    urlParams: ["name"]
})

/**
 * @param {Request}  req request contains client data
 * @param {Response} res response contains http methods
 */
async function templateHandler(req, res) {
    
    let db = link.get("main")
    let [count, err] = await db.table("template").where({name:req.params.name}).deleteHard()
    if(err) res.status(400).send()
    else if (count == 0) res.status(404).send()
    else res.status(204).send()//success

}
