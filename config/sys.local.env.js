const { npEnv } = require("@nodespull/core")


/**
 * dump vars into node.js process.env
 */
npEnv.process.loadVars(forTags = [], {
    db_selector: "main",
    db_database: "patest-db",
    db_host: "localhost",
    db_port: 3300,
    db_username: "root",
    db_password: "pass"
})